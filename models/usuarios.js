module.exports = function() {
	var db = require('./../libs/database')();
	var Schema = require('mongoose').Schema;

	var usuario = Schema({
		nome: String,
		email: String
	})

	return db.model('usuarios', usuario);
}